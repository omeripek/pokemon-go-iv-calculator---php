<?php
include("dustLevel.php");
//The file included above have arrays that we'll use to make sure valid inputs were given before doing any calculations.
if(isset($_POST["species"]) AND isset($_POST["cp"]) AND isset($_POST["dust"]) AND isset($_POST["hp"]) AND (array_key_exists(htmlspecialchars($_POST["dust"]),$dustLevel)) AND is_numeric(htmlspecialchars($_POST["cp"])) AND is_numeric(htmlspecialchars($_POST["hp"]))){
//This if statement will run the code if valid entries are given.
	include("stats.php");
	include("cpmLevel.php");
	include("evolutions.php");
	//Other arrays of information to help us out.
	$species = htmlspecialchars($_POST["species"]);
	//Sets the species we are calculating.
	if(array_key_exists($species,$evolutions) AND ($evolutions[$species]!="None")){
	//If this Pokemon has an evolution, run the following:
		$evolvedSpec = $evolutions[$species];
		//Set the species used in refining and evolution calculations to the Pokemon's evolution.
	} else {
	//If this Pokemon has no evolution, then run the following:
		$evolvedSpec = $species;
		//Set the species used in refining and evolution to the Pokemon itself.
	}
	//Finds the evolved species for the species given.
	if($evolvedSpec == "SpecialCon"){
		$evolvedSpec = "Vaporeon";
	}
	//An Eevee returns a special condition variable that we set to Vaporeon.  Adding Flareon and Jolteon would be easy, but not on my priority list.
	$cp = htmlspecialchars($_POST["cp"]);
	//Sets the CP.
	$dust = htmlspecialchars($_POST["dust"]);
	//Sets how much stardust a Power Up would take.
	$hp = htmlspecialchars($_POST["hp"]);
	//Sets the max HP a Pokemon displays.
	$stamina = $stats[$species][0];
	//Finds the Pokmeon's base stamina stat.
	$minLevelExtr = $dustLevel[$dust][0];
	//Uses the dust to find the minimum possible level for a Pokemon.
	$maxLevelExtr = $dustLevel[$dust][1];
	//Uses the dust to find the maximum possible level for a Pokemon.
	$ivSets=array();
	//Builds an array for Possible IVs we will append to later.
	while($maxLevelExtr>=$minLevelExtr){
	//While the Max Level is greater than or equal to the Min Level the Pokemon could be, run the following:
		$ivTest = ivSet($cpmLevel["$maxLevelExtr"], $cp, $stats[$species], $hp);
		//Builds a Possible set of Atk, Def, and HP IVs using floor().
		$ivSets[]=$ivTest;
		//Save this set of IVs.
		if(htmlspecialchars($_POST["submit"])=="Refine"){
		//Check if the "Refine" button was hit.  If this is the case, we need to enable the "round" option in ivSet. (Read the comments in the ivSet function to see why.)
			$ivTest2 = ivSet($cpmLevel["$maxLevelExtr"], $cp, $stats[$species], $hp, "round");
			//Finds a potential second set of IVs.
			$ivSets[]=$ivTest2;
			//Saves this potential set of IVs.
		}
		$maxLevelExtr=$maxLevelExtr-0.5;
		//Decrease the Max Level the Pokemon could be by 0.5 (since half levels affect what CP the Pokemon will be, we want to test every half level between the max/min levels.
	}
	$maxLevelExtr=$dustLevel[$dust][1];
	//Reset the Max Level to its original value.
	echo "Possible IV sets:<br />";
	//Display this message to the user.
	$finalIVs=array();
	//The finalIVs array will allow us to save the final list of IV sets.
	$minLevel = $dustLevel[$dust][0];
	//Sets the minimum possible level as a seperate variable for us to use.
	$maxLevel = $dustLevel[$dust][1];
	//Sets the maximum possible level as a seperate varialbe for us to use.
	$minIV = ivSet($cpmLevel["$maxLevel"], $cp, $stats[$species], $hp);
	//Calculates the minimum possible IVs.
	$maxIV = ivSet($cpmLevel["$minLevel"], $cp, $stats[$species], $hp);
	//Calculates the maximum possible IVs.
	$i = 0;
	//Sets the "i" variable to 0, so we can use it as a counter (mostly for debugging).
	while(($i<4) AND (($minIV[2]>15) OR ($minIV[2]<0))){
	//While the mimimum IV set's HP IV is greater than 15 or less than 0, and "i" is less than 4 (so we don't get stuck in an infinite loop due to error), run the following:
		$maxLevel+=0.5;
		/*Increase the max level by half.  This is because, the higher the level for the same stat, the lower the IVs will be.
		Here is a loose example to prove this: Imagine if CP divided by level equaled potenial IV power using the equation cp/l=iv
		If we are testing a CP 12 Pokemon for levels 2 and 3, the possible IV power would be:
		12/2=6 <- level 2 has higher IV potential
		12/3=4 <- level 3 has lower IV potential
		Because of this, raising the level will get our HP IV below 15.
		The HPIV<0 check was mostly for debug, but it's nice to keep in there for mathematical correction.
		*/
		$minIV = ivSet($cpmLevel["$maxLevel"], $cp, $stats[$species], $hp);
		//Calculates the minimum IVs.  Note that using the logic above where a higher level gives lower IV potential, we use the max level to find min IVs.
		$i++;
		//Adds 1 to "i" for debug to make sure the loop doesn't last forever in case of error.
	}
	$i = 0;
	//Resets "i" to use as a debug/safety counter.
	while(($i<4) AND (($maxIV[2]>15) OR ($maxIV[2]<0))){
	//While the max IV set's HP IV is greater than 15 or less than 0, and "i" is less than 4, run the following:
		$minLevel+=0.5;
		//Increase the minimum level the Pokemon can be.
		$maxIV = ivSet($cpmLevel["$minLevel"], $cp, $stats[$species], $hp);
		//Calculate the max IV set possible.
		$i++;
		//Increase "i" by 1.
	}
	$i = 0;
	//Resets "i" for debug/safety counter.
	while(($i<4) AND (($maxIV[0]+$maxIV[1])<($minIV[0]+$minIV[1]))){
	//While the Max Atk/Def IVs added are lower than the Min Atk/Def IVs, and "i" is less than 4, run the following:
		$minLevel-=0.5;
		//Decrease the min level to increase the max IVs.
		$maxIV = ivSet($cpmLevel["$minLevel"], $cp, $stats[$species], $hp);
		//Recalculate the max IVs.
		$i++;
		//Increase the "i" counter by 1.
	}
	if(($minIV[2]>=0) AND ($minIV[2]<=15)){
	//Validates the HP IV.
		$finalIVs[]=$minIV;
		//Saves this potential IV set.
	}
	if(($maxIV[2]>=0) AND ($maxIV[2]<=15)){
	//Validates the HP IV.
		$finalIVs[]=$maxIV;
		//Saves this potential IV set.
	}
	//Set the second entry of finalIVs to the maximum IVs possible.
	$levels=array();
	//Start an array for the levels that correspond to the finalIVs.
	$levels[]=$maxLevel;
	//Add the max level used to calculate the min IVs.
	$levels[]=$minLevel;
	//Add the min level used to calculate the max IVs.
	$i = 0;
	//Reset "i" to 0 for debug/safety counter.
	while(($minLevel!=($maxLevel-0.5)) AND $i<4){
	//This next part calculates all possible IV sets in between the max/min levels we found possible.
	//While the minimum level doesn't equal half a level subtracted from the max level, and "i" is less than 4, run the following:
		$newLevel = $maxLevel-0.5;
		//Sets the level we are testing.
		$newIV = ivSet($cpmLevel["$newLevel"], $cp, $stats[$species], $hp);
		//Sets this level's IV set.
		if(htmlspecialchars($_POST["submit"])=="Refine"){
		//Checks if the "Refine button was used so we know to check for both floor() values and round() values.
			$newIV2 = ivSet($cpmLevel["$newLevel"], $cp, $stats[$species], $hp, "round");
			//Sets this potential IV set.
		}
		if(($newIV[2]>=0) AND ($newIV[2]<=15)){
		//Makes sure the HP IV in this set is valid.
			$finalIVs[]=$newIV;
			//Saves this IV set.
			$levels[]=$newLevel;
			//Saves the corresponding level.
		}
		if(isset($newIV2)){
		//If the newIV2 variable exists (meaning that "Refine" was used), run the following:
			if(($newIV2[2]>=0) AND ($newIV[2]<=15) AND (($newIV[0]!=$newIV2[0]) OR ($newIV[1]!=$newIV2[1]) OR ($newIV[2]!=$newIV2[2]))){
			//Check is newIV2 has a proper HP IV and that it doesn't equal the newIV variable.  This is because their only difference is a floor() and round() function,
			//so they could be equal, and we don't want to save/display to identical IV sets.
				$finalIVs[]=$newIV2;
				//Save the newIV2
				$levels[]=$newLevel;
				//And the accociated level.
			}
		}
		$maxLevel-=0.5;
		//Lowers max level to what we just tested, so that the loop will look for the half level below what we just tested.
		$i++;
		//Adds 1 to the "i" counter.
	}
	$maxLevel=$dustLevel[$dust][1];
	//Sets the max level to what it should be.
	if(htmlspecialchars($_POST["submit"])!="Refine"){
	//Checks to see if the data sent to this page wasn't sent via the "Refine" button.
		foreach($finalIVs as $ivCalc){
		//Cycle through all IV sets.
			echo "Atk IV: ".$ivCalc[0]." Def IV: ".$ivCalc[1]." HP IV: ".$ivCalc[2]."<br />";
			//And display them.
		}
	}
	if(htmlspecialchars($_POST["submit"])=="Evolve"){
	//If the "Evolve" button was used, then run the following:
		$l=0;
		//This is a level counter corresponding to the IV sets.
		foreach($finalIVs as $testIVs){
		//Loops through every IV set.
			if(!isset($minIVs)){
			//If the minIVs variable isn't set, then it sets it below.
				$minIVs=$testIVs;
				$maxIVs=$testIVs;
				//It also sets the max as the same level, in case that IV set ends up being the max instead, or there is only one set.
				$minLevels=$levels[$l];
				$maxLevels=$levels[$l];
				//The levels corresponding to each are also set.
			} else {
				if(($minIVs[0]+$minIVs[1]+$minIVs[2])>($testIVs[0]+$testIVs[1]+$testIVs[2])){
				//This tests if the current min IV set is larger than the current testing set, and sets the new minimums if this is true.
					$minIVs=$testIVs;
					$minLevels=$levels[$l];
				} else if(($maxIVs[0]+$maxIVs[1]+$maxIVs[2])<($testIVs[0]+$testIVs[1]+$testIVs[2])){
				//This tests if the current max IV set is smaller than the current testing set, and gets the new maximums if this is true.
					$maxIVs=$testIVs;
					$maxLevels=$levels[$l];
				}
			}
			$l++;
			//Level counter increase.
		}
		$newCPMin = floor(reverseStats($minIVs, $cpmLevel["$minLevels"], $stats[$evolvedSpec]));
		$newCPMax = floor(reverseStats($maxIVs, $cpmLevel["$maxLevels"], $stats[$evolvedSpec]));
		//The new CPs for evolved Pokemon have been set, but due to glitches with certain Pokemon *cough*Pidgey*cough*, we'll check later to see which is really lower.
		//For some reason, the CP range is still accurate, even if the IVs aren't, and I haven't looked into the reasoning for why this is.
		echo "<br />Your ".$evolvedSpec." will have<br />";
		//Dispalying this to the client makes the following information sound better.
	} else if(htmlspecialchars($_POST["submit"])=="Power Up"){
	//If the "Power Up" button was used, then run the following:
                $l=0;
                //This is a level counter corresponding to the IV sets.
                foreach($finalIVs as $testIVs){
                //Loops through every IV set.
                        if(!isset($minIVs)){
                        //If the minIVs variable isn't set, then it sets it below.
                                $minIVs=$testIVs;
                                $maxIVs=$testIVs;
                                //It also sets the max as the same level, in case that IV set ends up being the max instead, or there is only one set.
                                $minLevels=$levels[$l];
                                $maxLevels=$levels[$l];
                                //The levels corresponding to each are also set.
                        } else {
                                if(($minIVs[0]+$minIVs[1]+$minIVs[2])>($testIVs[0]+$testIVs[1]+$testIVs[2])){
                                //This tests if the current min IV set is larger than the current testing set, and sets the new minimums if this is true.
                                        $minIVs=$testIVs;
                                        $minLevels=$levels[$l];
                                } else if(($maxIVs[0]+$maxIVs[1]+$maxIVs[2])<($testIVs[0]+$testIVs[1]+$testIVs[2])){
                                //This tests if the current max IV set is smaller than the current testing set, and gets the new maximums if this is true.
                                        $maxIVs=$testIVs;
                                        $maxLevels=$levels[$l];
                                }
                        }
                        $l++;
                        //Level counter increase.
                }
		$maxLevels-=1.5;
		//Subtract 1.5 from the level due to previous calculations to ensure we have the right power up level.
		$minLevels-=1.5;
		//Subtract 1.5 from the level due to previous calculations to ensure we have the right power up level.
		$newCPMax = floor(reverseStats($maxIVs, $cpmLevel["$maxLevels"], $stats[$species]));
		//Calculates the new max level's CP.
		$newCPMin = floor(reverseStats($minIVs, $cpmLevel["$minLevels"], $stats[$species]));
		//Calculates the new min level's CP.
		echo "<br />Your " .$species." will have<br />";
		//Displaying this gives info on what the next set of information means.
		setcookie("finalIVs", json_encode($finalIVs), time() + (86400), "/");
		//Saves the list of potential IVs as a cookie.
		setcookie("levels", json_encode($levels), time() + (86400), "/");
		//Saves the list of corresponding levels as a cookie.
	} else if((htmlspecialchars($_POST["submit"])=="Refine") AND isset($_COOKIE["finalIVs"]) AND isset($_COOKIE["levels"])){
	//If the "Refine button was used and the finalIVs and levels cookies are set, then run the following:
		$compareIVs=array();
		//Create the compareIVs array.
		$compareLevels=array();
		//Create the compareLevels array.
		$lastIVs=json_decode($_COOKIE["finalIVs"]);
		//Set the lastIVs to the possible IV sets found in the last session.
		$levels=json_decode($_COOKIE["levels"]);
		//Set the levels to the possible corresponding levels found in the last session.
		foreach($finalIVs as $firstIVs){
		//For every possible IV set found in this session, run the following:
			$l = 0;
			//Set a counter to keep track of last session's determined levels.
			foreach($lastIVs as $thisIVs){
			//For every possible IV set found last session, run the following:
				if(($firstIVs[0]==htmlspecialchars($thisIVs[0])) AND ($firstIVs[1]==htmlspecialchars($thisIVs[1])) AND ($firstIVs[2]==htmlspecialchars($thisIVs[2]))){
				//If all IVs from both sessions match, run the following:
					echo "Atk IV: ".$firstIVs[0]." Def IV: ".$firstIVs[1]." HP IV: ".$firstIVs[2]."<br />";
					//Display the IV set matches
					$compareIVs[]=$firstIVs;
					//And save them in the compareIVs array.
					$compareLevels[]=htmlspecialchars($levels[$l])-0.5;
					//Save the corresponding levels as well.  The levels have half a level subtracted from it, since these levels are based on
					//the levels used to predict the future CP of a Pokemon (which means we previously increased the level by 0.5).
				}
				$l++;
				//Increase the level counter.
			}
		}
		if($species==$evolvedSpec){
		//Checks if this refining should display evolution or Power Up CPs.  If the species equals the evolution, then the species has no evolution, so a Power Up CP will be shown.
			$firstLevel=$compareLevels[0]+1.5;
			/*Sets one of the levels corresponding with an IV set.  Sometimes, there will only be one IV set, but in the case of two, we don't know
			which one will be higher or lower, so we just set this variable as the first level.
			While setting this, we add three half levels (1.5 levels) to the total.  This is because we need to take into account the past powerups and future CP power up.
			*/
			$secondLevel=$compareLevels[count($compareIVs)-1]+1.5;
			//Sets the second variable as the other level plus one.
		} else {
		//Sets the levels for evolution instead of Power Up.
			$firstLevel=$compareLevels[0];
			$secondLevel=$compareLevels[count($compareIVs)-1];
		}
	        $newCPMax = floor(reverseStats($compareIVs[0], $cpmLevel["$firstLevel"], $stats[$evolvedSpec]));
		//This sets one extreme CP, but we don't know if it's the max or min CP, because of the way the IVs are stored.
                $newCPMin = floor(reverseStats($compareIVs[count($compareIVs)-1], $cpmLevel["$secondLevel"], $stats[$evolvedSpec]));
		//This sets another extreme.  If there's only one, they will be equal.
		if($species==$evolvedSpec){
		//Checks if the Pokemon has an evolution.  If not, A Power Up calculation will be applied instead of Evolution, so we need to tell the user.
			echo "<br />Your ".$evolvedSpec." will Power Up to<br />";
			//Dispalying this to the user tells them what the following information means.
		} else {
	                echo "<br />Your ".$evolvedSpec." will have<br />";
			//Displaying this to the user tells them what the following information means.
		}
	}
	if($newCPMax>=$newCPMin){
	//Add notes here.
		$maxCP=$newCPMax;
		$minCP=$newCPMin;
	} else {
		$minCP=$newCPMax;
		$maxCP=$newCPMin;
	}
	echo "Minimum: ".$minCP." CP!<br />";
	//This will display the new min CP.
	echo "Maximum: ".$maxCP." CP!<br />";
	//This will display the new max CP.
}

function possibleIVs($mixedStats,$baseStats){
/*This function finds possible IVs, given the mixed stats and base stats of a Pokemon.
The equation for using the Atk IV, Def IV, and base stats to find the mixed stats is the followng:
((BaseAtk+AtkIV)^2)*(BaseDef+DefIV)=MixedStats
(How to find the MixedStats is found in a function below.)
The easiest way to do this was mentioned by /u/homu on /u/Shiesu's thread in /r/PokemonGoDev:
Create a table of all possible combinations of stats for a Pokemon
starting by multiplying the Base Atk squared and Base Def, then adding 1 stat (for the IV)
to the Base Def until all Base Atk with 0 IVs have multiplied with all Base Def + IV combinations.
After that, add 1 stat (for the IV) to the Base Atk, and run through the Def combinations again.
Do this until all Stats have been calculated, then find the Mixed Stat closest (rounding down) to
the already calculated Mixed Stats we have.  From there, you have the Atk and Def stat values.
Subtracting the already known base stats from each one will give you the IVs.
*/
	$atk = $baseStats[1];
	//Sets the Pokemon's base Atk Stat.
	$def = $baseStats[2];
	//Sets the Pokemon's base Def Stat.
	$lastStats=0;
	//This variable keeps track of the last mixed stats tested so we can compare them to the current mixed stats.
	$ivs = array(0,0);
	//This is an array of IVs.  We set the first one as 0 for the Atk IV and the second as 0 for the Def IV.  This gives us a comparison for the real stats.
	for($x = 0; $x<=15; $x++){
	//Here, the variable "x" represents Atk IV (even if adding debug code to this for a visual table puts it on the y-axis).
	//This loop says "For x = 0, run through this loop while x is less than or equal to 15, adding 1 to x during each loop.
		for($y = 0; $y<=15; $y++){
		//The variable "y" represents Def IV.  For y starting at 0, going while y is less than or equal to 15, and adding 1 during each loop.
			$ivStats = pow(($atk+$x),2)*($def+$y);
			//Calculates the Mixed Stats for these IVs.
			if(($ivStats>=$mixedStats) && ($mixedStats>$lastStats)){
			//If the mixedStats given to this function are less than the current ivStats, but higher than the lastStats, we have an IV match.
				$ivs = array($x,$y);
				//Set the ivs variable to the new Atk/Def IV values.
			}
			$lastStats=$ivStats;
			//Sets the stats just tested as the last stats for comparison during the next loop.
		}
	}
	return $ivs;
	//Return the final determined IVs to whatever called this function.
}

function comStats($hp, $cpm, $cp, $round="null"){
/*This function computes the HP stat and the mixed stats used in the function above.
The equation to convert the CP and CP Multiplier into the mixed stats is the following:
MixedStats=((10*cp/cpm^2)^2)/stamina
This function also has an optional argument to use the round() or floor() functions to calculate IVs.
The reason I did this is because the game has been said to use a modified version of the floor() function for rounding.
When testing, I found that both round() and floor() can produce accurate results in different situations.
For example, a Pokemon's IVs being calculated the first time using floor() can be correct, however after Powering Up,
the correct IVs might be found using round(), as floor() would produce incorrect results.  By adding this option
to this function, when finding possible IV matches, if none exist, I can switch from using floor() to round() and try
to find a matching set of IVs with the different rounding method.
*/
	$cpStat = (10*$cp/pow($cpm,2));
	//Find the first part of the equation.  Setting this aside makes the code look a bit more tidy.
	if($round=="null"){
	//Tests is the function should use round() or floor().
		$stamina = floor($hp/$cpm);
		//This finds the stamina stat, which is the Base HP+HP IV.  Since we aren't finding the IV here, we don't need the base stats of a Pokemon.
		$mixedStats = floor(pow($cpStat,2)/$stamina);
		//mixedStats is set to equal the equation used to find it.
	} else {
	//This next part is identical to the above lines, except it uses the round() function instead of floor().
		$stamina = round($hp/$cpm);
		$mixedStats=round(pow($cpStat,2)/$stamina);
	}
	return $mixedStats;
	//Return the mixed stats to whatever called this function.
}

function reverseStats($IVs, $cpm, $baseStats){
/*This reverses the stats to find the CP they will create.  Changing the CP Multiplier to the next Multiplier will simulate a Pokemon's Power Up.
Changing the multiplier's level (giving it a new multiplier) with the equation currentCpmLevel+(0.5*n), where "n" is the amount of Power Ups will simulate multiple Power Ups.
Changing the base stats to a Pokemon's evolution will simulate the evolved Pokemon's CP.
Using algebra, you can take the equation to give the mixed stats (Atk^2*Def) and solve for CP instead, giving:
CP = ((Attack*(Defense^0.5)*(Stamina^0.5)*CPM^2)/10
Note that setting the power of a number, x, to a fraction (0.5 = 1/2) means setting the power of "x"
to the exponent's numerator (1 in this case), and taking the n-th root of x where "n" is the exponent's denominator (2 in this case).
so x^(1/2) = squareRoot(x^1).  (Setting the power of a number to 1 will equal the base number).
*/
	$staminaStat = $IVs[2]+$baseStats[0];
	//Sets the new HP stat to a Pokemon's base HP plus the HP IV given.
	$atkStat = $IVs[0]+$baseStats[1];
	//Sets the Atk stat to a Pokemon's base Atk plus the Atk IV given.
	$defStat = $IVs[1]+$baseStats[2];
	//Sets the Def stat to a Pokemon's base Def plus the Def IV given.
	$newCP = ($atkStat*pow($defStat,"0.5")*pow($staminaStat,"0.5")*pow($cpm,"2"))/10;
	//Creates a new CP with these variables.
	return $newCP;
	//Returns the new CP to whatever called this equation.
}

function ivSet($cpm, $cp, $baseStats, $hp, $round="null"){
/*This calculates all the possible IVs using the previous functions using the variables given.
Because comStats uses an optional fourth argument to use round() instead of floor() (read the comStats
notes to find out why), this function, which gets called from the main script, also has to include it.
*/
        $testStats = comStats($hp,$cpm,$cp,$round);
	//Gets the mixed stats of a CP Multiplier, CP, and displayed max HP.
        $ivTest = possibleIVs($testStats,$baseStats);
	//Gets the IVs based off the given mixed stats.
        $hpIVTest = floor($hp/$cpm)-$baseStats[0];
	//Finds the HP IV based on the CP Multiplier, Displayed Max HP, and HP Base Stat of a given Pokemon.
	$resInfo=array($ivTest[0],$ivTest[1],$hpIVTest);
	//Creates an array of IVs in this order: AtkIV, DefIV, HPIV.
	return $resInfo;
	//Returns the IV array to whatever called this function.
}

?>
