<?php
$stats = array(
"Dragonite"=>array(182,250,212),
"Lapras"=>array(260,186,190),
"Vaporeon"=>array(260,186,168),
"Snorlax"=>array(320,180,180),
"Arcanine"=>array(180,230,180),
"Slowbro"=>array(190,184,198),
"Exeggutor"=>array(190,232,164),
"Muk"=>array(210,180,188),
"Blastoise"=>array(158,186,222),
"Charizard"=>array(156,212,182),
"Nidoqueen"=>array(180,184,190),
"Poliwrath"=>array(180,180,202),
"Gyarados"=>array(190,192,196),
"Venusaur"=>array(160,198,200),
"Wigglytuff"=>array(280,168,108),
"Golduck"=>array(160,194,176),
"Nidoking"=>array(162,204,170),
"Clefable"=>array(190,178,178),
"Rhydon"=>array(210,166,160),
"Hypno"=>array(170,162,196),
"Omastar"=>array(140,180,202),
"Tentacruel"=>array(160,170,196),
"Golem"=>array(160,176,198),
"Dewgong"=>array(180,156,192),
"Vileplume"=>array(150,202,190),
"Pidgeot"=>array(166,170,166),
"Victreebel"=>array(160,222,152),
"Starmie"=>array(120,194,192),
"Flareon"=>array(130,238,178),
"Golbat"=>array(150,164,164),
"Machamp"=>array(180,198,180),
"Ninetales"=>array(146,176,194),
"Kangaskhan"=>array(210,142,178),
"Weezing"=>array(130,190,198),
"Aerodactyl"=>array(160,182,162),
"Tauros"=>array(150,148,184),
"Gengar"=>array(120,204,156),
"Cloyster"=>array(100,196,196),
"Sandslash"=>array(150,150,172),
"Rapidash"=>array(130,200,170),
"Magmar"=>array(130,214,158),
"Venomoth"=>array(140,172,154),
"Seaking"=>array(160,172,160),
"Scyther"=>array(140,176,180),
"Alakazam"=>array(110,186,152),
"Pinsir"=>array(130,184,186),
"Dragonair"=>array(122,170,152),
"Kabutops"=>array(120,190,190),
"Marowak"=>array(120,140,202),
"Raichu"=>array(120,200,154),
"Parasect"=>array(120,162,170),
"Jolteon"=>array(130,192,174),
"Wartortle"=>array(118,144,176),
"Persian"=>array(130,156,146),
"Porygon"=>array(130,156,158),
"Tangela"=>array(130,164,152),
"Seadra"=>array(110,176,150),
"Electabuzz"=>array(130,198,160),
"Magneton"=>array(100,186,180),
"Kingler"=>array(110,178,168),
"Jynx"=>array(130,172,134),
"Lickitung"=>array(180,126,160),
"Ivysaur"=>array(120,156,158),
"Gloom"=>array(120,162,158),
"Arbok"=>array(120,166,166),
"Electrode"=>array(120,150,174),
"Dodrio"=>array(120,182,150),
"Hitmonchan"=>array(100,138,204),
"Machoke"=>array(160,154,144),
"Primeape"=>array(130,178,150),
"Fearow"=>array(130,168,146),
"Weepinbell"=>array(130,190,110),
"Beedrill"=>array(130,144,130),
"Butterfree"=>array(120,144,144),
"Mr. Mime"=>array(80,154,196),
"Hitmonlee"=>array(100,148,172),
"Graveler"=>array(110,142,156),
"Nidorina"=>array(140,132,136),
"Slowpoke"=>array(180,110,110),
"Nidorino"=>array(122,142,128),
"Charmeleon"=>array(116,160,140),
"Poliwhirl"=>array(130,132,132),
"Pidgeotto"=>array(126,126,122),
"Raticate"=>array(110,146,150),
"Rhyhorn"=>array(160,110,116),
"Seel"=>array(130,104,138),
"Ponyta"=>array(100,168,138),
"Haunter"=>array(90,172,118),
"Clefairy"=>array(140,116,124),
"Grimer"=>array(160,124,110),
"Growlithe"=>array(110,156,110),
"Drowzee"=>array(120,104,140),
"Psyduck"=>array(100,132,112),
"Omanyte"=>array(70,132,160),
"Eevee"=>array(110,114,128),
"Exeggcute"=>array(120,110,132),
"Cubone"=>array(100,102,150),
"Venonat"=>array(120,108,118),
"Kadabra"=>array(80,150,112),
"Oddish"=>array(90,134,130),
"Dugtrio"=>array(70,148,140),
"Jigglypuff"=>array(230,98,54),
"Chansey"=>array(500,40,60),
"Bulbasaur"=>array(90,126,126),
"Squirtle"=>array(88,112,142),
"Koffing"=>array(80,136,142),
"Dratini"=>array(82,128,110),
"Bellsprout"=>array(100,158,78),
"Machop"=>array(140,118,96),
"Tentacool"=>array(80,106,136),
"Kabuto"=>array(60,148,142),
"Staryu"=>array(60,130,128),
"NidoranF"=>array(110,100,104),
"Paras"=>array(70,122,120),
"Farfetchd"=>array(104,138,132),
"Sandshrew"=>array(100,90,114),
"Goldeen"=>array(90,112,126),
"Charmander"=>array(78,128,108),
"Onix"=>array(70,90,186),
"Voltorb"=>array(80,102,124),
"NidoranM"=>array(92,110,94),
"Meowth"=>array(80,104,94),
"Poliwag"=>array(80,108,98),
"Ekans"=>array(70,112,112),
"Vulpix"=>array(76,106,118),
"Mankey"=>array(80,122,96),
"Magnemite"=>array(50,128,138),
"Geodude"=>array(80,106,118),
"Horsea"=>array(60,122,100),
"Krabby"=>array(60,116,110),
"Pikachu"=>array(70,124,108),
"Pidgey"=>array(80,94,90),
"Doduo"=>array(70,126,96),
"Shellder"=>array(60,120,112),
"Gastly"=>array(60,136,82),
"Zubat"=>array(80,88,90),
"Spearow"=>array(80,102,78),
"Metapod"=>array(100,56,86),
"Rattata"=>array(60,92,86),
"Kakuna"=>array(90,62,82),
"Abra"=>array(50,110,76),
"Caterpie"=>array(90,62,66),
"Weedle"=>array(80,68,64),
"Diglett"=>array(20,108,86),
"Magikarp"=>array(40,42,84)
);
?>
