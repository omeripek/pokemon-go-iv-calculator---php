<?php
include("cp/specs.php");
ob_start();
echo '<html>
	<head>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Syncopate" />
		<link rel="stylesheet" href="cp/style.css">
	</head>
	<body>
		<center>
			<div class="title">Pokemon Go Calculator</div>
				<h2>Calculate IVs and get closer approximations of what your Pokemon CP will be after Evolution...somtimes even a guaranteed CP.</h2>
				<h2>Instructions: Use "Evolve" to determine possible CP range after evolution.  Use "Power Up" to determine CP after Powering Up.  Use "Refine" after "Power Up" to get a closer estimate on an evolution or Power Up CP.</h2>';
echo "				<h3>Known Bugs/Limitations: Pidgey and Rattata will often give IVs that aren't valid (perhaps due to incorrect base stats), so they won't calcuate correctly.  You can only Refine stats one time after a Power Up on the same Pokemon.  Finally, due to the game's irregular rounding method, CP values given may be 1 off.  After refining a Pokemon, it is safe to say that the IVs shown will be their IV set, but mathematically, an uncaught rounding error could potentially happen in the future.</h1>";
echo '			<div class="post">
				Select a Pokemon to Evolve:
				<br />
				<form action="index.php" method="post">
				<select class="gbtnHollow" name="species">
					<option value="">Select Pokemon</option>';
				foreach($specs as $pokemon){
					echo '<option value="'.$pokemon.'">'.$pokemon.'</option>';
				}
echo '
				</select>
				<br />
				Enter Pokemon CP:
				<br />
				<input class="gbtnHollow" type="text" name="cp">
				<br />
				Enter Max HP:
				<br />
				<input class="gbtnHollow" type="text" name="hp">
				<br />
				Stardust to Power Up?
				<br />
				<input class="gbtnHollow" type="text" name="dust">
				<br />
				<input class="gbtnHollow" type="submit" name="submit" value="Power Up" />
				<input class="gbtnHollow" type="submit" name="submit" value="Refine" />
				<input class="gbtnHollow" type="submit" name="submit" value="Evolve" />
				</form>
				<br />';
include("cp/cp.php");
echo '
		</div>
	</body>
</html>
';
ob_end_flush();
?>
